module com.example.proyectoedgarruiz {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.xml;


    opens com.example.proyectoedgarruiz to javafx.fxml;
    exports com.example.proyectoedgarruiz;
}