package com.example.proyectoedgarruiz;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

public class Metodos {

    public static ArrayList<Float> sumaTotalBruto = new ArrayList<>();
    public static ArrayList<Float> totalIva = new ArrayList<>();
    public static ArrayList<Float> suma = new ArrayList<>();

    public static void tamanoField(final TextField campoTexto, final int tamanoMaximo) {
        campoTexto.lengthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable,
                                Number valorAnterior, Number valorActual) {
                if (valorActual.intValue() > valorAnterior.intValue()) {

                    if (campoTexto.getText().length() >= tamanoMaximo) {
                        campoTexto.setText(campoTexto.getText().substring(0, tamanoMaximo));
                    }
                }
            }
        });
    }

    public static void onlyNumbers(TextField tf) {
        tf.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    tf.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
    }

    public static void onlyDecimals(TextField tf) {
        tf.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*(\\.\\d*)?")) {
                    tf.setText(oldValue);
                }
            }
        });

    }

    //TODO toda la información sobre el 'Tax' del xml
    public static void Tax(Document doc, int i, Element invoice,
                           Element taxesOutputs, ArrayList<String> ivaList, ArrayList<Float> cantList,
                           ArrayList<Float> precioList) {
        float sum = 0;
        float total = TotalBruto(sum, i, cantList, precioList);

        sumaTotalBruto.add(total);

        Node tax = doc.getFirstChild();
        Element taxx = doc.createElement("Tax");
        tax.appendChild(taxx);
        invoice.appendChild(taxx);
        taxesOutputs.appendChild(taxx);
        Element taxTypeCode = doc.createElement("TaxTypeCode");
        taxTypeCode.setTextContent("01");
        taxx.appendChild(taxTypeCode);
        Element taxRate = doc.createElement("TaxRate");
        if(ivaList.get(i) == "21%") {
            taxRate.setTextContent("21.00");
        }else if(ivaList.get(i) == "10%") {
            taxRate.setTextContent("10.00");
        }else if(ivaList.get(i) == "4%") {
            taxRate.setTextContent("4.00");
        }else if(ivaList.get(i) == "SIN IVA") {
            taxRate.setTextContent("0.00");
        }
        taxx.appendChild(taxRate);

        //TODO datos preciosos tax (total bruto y total con iva)
        Node taxable = doc.getFirstChild();
        Element taxableBase = doc.createElement("TaxableBase");
        taxable.appendChild(taxableBase);
        invoice.appendChild(taxableBase);
        taxesOutputs.appendChild(taxableBase);
        taxx.appendChild(taxableBase);
        Element totalAmount = doc.createElement("TotalAmount");
        totalAmount.setTextContent(String.valueOf(total));
        taxableBase.appendChild(totalAmount);
        Node taxAm = doc.getFirstChild();
        Element taxAmount = doc.createElement("TaxAmount");
        taxAm.appendChild(taxAmount);
        invoice.appendChild(taxAmount);
        taxesOutputs.appendChild(taxAmount);
        taxx.appendChild(taxAmount);
        Element totalAmount2 = doc.createElement("TotalAmount");
        totalAmount2.setTextContent(String.valueOf(TotalIVA(total, i, cantList, precioList, ivaList)));
        taxAmount.appendChild(totalAmount2);
    }

    //TODO toda la información sobre el 'Tax de un Item' del xml
    public static void TaxItem(Document doc, int i, Element invoice,
                           Element invoiceLine, ArrayList<String> ivaList, ArrayList<Float> cantList,
                           ArrayList<Float> precioList) {
        float sum = 0;
        float total = TotalBruto(sum, i, cantList, precioList);
        float iva = TotalIVA(total, i, cantList, precioList, ivaList);

        float suma = total + iva;

        Node taxesOut = doc.getFirstChild();
        Element taxesOutputs = doc.createElement("TaxesOutputs");
        taxesOut.appendChild(taxesOutputs);
        invoice.appendChild(taxesOutputs);
        invoiceLine.appendChild(taxesOutputs);
        Node tax = doc.getFirstChild();
        Element taxx = doc.createElement("Tax");
        tax.appendChild(taxx);
        invoice.appendChild(taxx);
        taxesOutputs.appendChild(taxx);
        Element taxTypeCode = doc.createElement("TaxTypeCode");
        taxTypeCode.setTextContent("01");
        taxx.appendChild(taxTypeCode);
        Element taxRate = doc.createElement("TaxRate");
        if(ivaList.get(i) == "21%") {
            taxRate.setTextContent("21.00");
        }else if(ivaList.get(i) == "10%") {
            taxRate.setTextContent("10.00");
        }else if(ivaList.get(i) == "4%") {
            taxRate.setTextContent("4.00");
        }else if(ivaList.get(i) == "SIN IVA") {
            taxRate.setTextContent("0.00");
        }
        taxx.appendChild(taxRate);

        //TODO datos preciosos tax (total bruto y total con iva)
        Node taxable = doc.getFirstChild();
        Element taxableBase = doc.createElement("TaxableBase");
        taxable.appendChild(taxableBase);
        invoice.appendChild(taxableBase);
        invoiceLine.appendChild(taxableBase);
        taxx.appendChild(taxableBase);
        Element totalAmount = doc.createElement("TotalAmount");
        totalAmount.setTextContent(String.valueOf(total));
        taxableBase.appendChild(totalAmount);
        Node taxAm = doc.getFirstChild();
        Element taxAmount = doc.createElement("TaxAmount");
        taxAm.appendChild(taxAmount);
        invoice.appendChild(taxAmount);
        invoiceLine.appendChild(taxAmount);
        taxx.appendChild(taxAmount);
        Element totalAmount2 = doc.createElement("TotalAmount");
        totalAmount2.setTextContent(String.valueOf(suma));
        taxAmount.appendChild(totalAmount2);
    }

    //TODO toda la información sobre el 'Tax' del xml
    public static void Items(Document doc, int i, Element invoice,
                           Element items, ArrayList<String> ivaList, ArrayList<Float> cantList,
                           ArrayList<Float> precioList, ArrayList<String> descList) {
        float sum = 0;

        Node invo = doc.getFirstChild();
        Element invoiceLine = doc.createElement("InvoiceLine");
        invo.appendChild(invoiceLine);
        invoice.appendChild(invoiceLine);
        items.appendChild(invoiceLine);
        Element fileReference = doc.createElement("FileReference");
        invoiceLine.appendChild(fileReference);
        Element itemDescription = doc.createElement("ItemDescription");
        itemDescription.setTextContent(descList.get(i).toUpperCase());
        invoiceLine.appendChild(itemDescription);
        Element quantity = doc.createElement("Quantity");
        quantity.setTextContent(String.valueOf(cantList.get(i)));
        invoiceLine.appendChild(quantity);
        Element unitPriceWithoutTax = doc.createElement("UnitPriceWithoutTax");
        unitPriceWithoutTax.setTextContent(String.valueOf(precioList.get(i)));
        invoiceLine.appendChild(unitPriceWithoutTax);
        Element totalCost = doc.createElement("TotalCost");
        totalCost.setTextContent(String.valueOf(TotalBruto(sum, i, cantList, precioList)));
        invoiceLine.appendChild(totalCost);
        Element grossAmount = doc.createElement("GrossAmount");
        grossAmount.setTextContent(String.valueOf(TotalBruto(sum, i, cantList, precioList)));
        invoiceLine.appendChild(grossAmount);
        Metodos.TaxItem(doc, i, invoice, invoiceLine, ivaList, cantList, precioList);
        Element articleCode = doc.createElement("ArticleCode");
        invoiceLine.appendChild(articleCode);

    }

    //TODO total precio bruto de cada producto
    public static Float TotalBruto(float sum, int i, ArrayList<Float> cantList, ArrayList<Float> precioList){
        sum += cantList.get(i) * precioList.get(i);
        return sum;
    }

    //TODO precio total de cada producto con IVA
    public static Float TotalIVA(float total, int i, ArrayList<Float> cantList, ArrayList<Float> precioList, ArrayList<String> ivaList) {

        if (ivaList.get(i) == "21%") {
            total = total * 21 / 100;
            totalIva.add(total);
            return total;
        } else if (ivaList.get(i) == "10%") {
            total = total * 10 / 100;
            totalIva.add(total);
            return total;
        } else if (ivaList.get(i) == "4%") {
            total = total * 4 / 100;
            totalIva.add(total);
            return total;
        } else if (ivaList.get(i) == "SIN IVA") {
            total = total * 0 / 100;
            totalIva.add(total);
            return total;
        }
        return total;
    }

    public static float ivaSum(){
        float suma = 0;
        for(int i = 0; i < totalIva.size(); i++){
            suma += totalIva.size();
        }
        return suma;
    }

    public static Float Total(){
        float tot = 0;
        for(int i = 0; i < sumaTotalBruto.size(); i++){
            tot += sumaTotalBruto.get(i);
        }
        return tot;
    }

    public static void porcentajeIRPF(Element taxRate, ComboBox cbIRPF) {
        if(cbIRPF.getValue().equals("15%")) {
            taxRate.setTextContent("15.00");
        }else if(cbIRPF.getValue().equals("19%")){
            taxRate.setTextContent("19.00");
        }else if(cbIRPF.getValue().equals("7%")){
            taxRate.setTextContent("7.00");
        }
    }

    public static float calculoIRPF(float total1, ComboBox cbIRPF) {

        float totalIRPF = 0;
        if(cbIRPF.getValue().equals("15%")) {
            totalIRPF = total1 * 15 / 100;
            return totalIRPF;
        }else if(cbIRPF.getValue().equals("19%")){
            totalIRPF = total1 * 19 / 100;
            return totalIRPF;
        }else if(cbIRPF.getValue().equals("7%")){
            totalIRPF = total1 * 7 / 100;
            return totalIRPF;
        }
        return totalIRPF;
    }

    //TODO el nombre default del archivo
    public static String name () {
        Calendar c = Calendar.getInstance();
        int mes = c.get(Calendar.MONTH) + 1;
        String nombre = "FACTUE" + c.get(Calendar.DAY_OF_MONTH) + mes + c.get(Calendar.YEAR) +
                c.get(Calendar.HOUR_OF_DAY) + c.get(Calendar.MINUTE);
        return nombre;
    }

    //TODO guardar el archivo xml
    public static void save (Transformer transformer, DOMSource source, Stage stage) throws TransformerException {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML (.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);
        fileChooser.setInitialFileName(Metodos.name());
        File selectedFile = fileChooser.showSaveDialog(stage);
        if (selectedFile != null) {
            StreamResult result = new StreamResult(new File(String.valueOf(selectedFile)));
            transformer.transform(source, result);

            //TODO alerta de que ha descargado el archivo sin problemas
            String mensaje = "Se ha guardado correctamente";
            Alert alert = new Alert(Alert.AlertType.INFORMATION, mensaje, ButtonType.OK);
            alert.setTitle("Información!");
            alert.setHeaderText(mensaje);
            alert.setContentText("");
            alert.showAndWait();
        }
    }
}