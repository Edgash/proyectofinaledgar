package com.example.proyectoedgarruiz;

import javafx.application.HostServices;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import java.io.IOException;
import java.net.URL;
import java.util.*;

public class Controller implements Initializable {

    @FXML
    private TextField name, surname1, surname2, cif;

    @FXML
    private TextField province, city, direction, postalCode;

    @FXML
    private TextField numFact, details;

    @FXML
    private TextField iban1, iban2, iban3, iban4, iban5, iban6;

    @FXML
    private TextField cant, desc, precio;

    @FXML
    private DatePicker date;

    @FXML
    private ComboBox<String> personType, serieFact, cbIRPF, cbIVA;

    @FXML
    private CheckBox chIRPF;

    @FXML
    private Button btnSave, btnDelete, btnAdd;

    @FXML
    private TableView tabla;

    @FXML
    private TableColumn<?, Double> colCant;

    @FXML
    private TableColumn<?, String> colDesc;

    @FXML
    private TableColumn<?, String> colIVA;

    @FXML
    private TableColumn<?, Double> colPrecio;

    @FXML
    private ImageView img;

    @FXML
    private Hyperlink link;

    private Stage stage;


    @FXML
    void Save(ActionEvent event) {

        Producto product = new Producto();
        List <Float> cantList = new ArrayList<>();
        List <Float> precioList = new ArrayList<>();
        List <String> ivaList = new ArrayList<>();
        List <String> descList = new ArrayList<>();

        for (int i = 0; i < tabla.getItems().size(); i++) {
            product = (Producto) tabla.getItems().get(i);
            cantList.add(product.getCant());
            precioList.add(product.getPrecio());
            ivaList.add(product.getIva());
            descList.add(product.getDesc());
        }

        if(personType.getValue() == "Persona Física" && cif.getText() == "" || name.getText() == ""
                || direction.getText() == "" || postalCode.getText() == "" || city.getText() == ""
                || province.getText() == "" || numFact.getText() == "" || date.getValue() == null
                || details.getText() == "" || iban1.getText() == "" || iban2.getText() == "" || iban3.getText() == ""
                || iban4.getText() == "" || iban5.getText() == "" || iban6.getText() == "" || cantList.size() <= 0) {
            String mensaje = "No se ha podido guardar correctamente.";
            Alert alert = new Alert(Alert.AlertType.ERROR, mensaje, ButtonType.OK);
            alert.setTitle("Error!");
            alert.setHeaderText(mensaje);
            alert.setContentText("");
            alert.showAndWait();

        } else {

            try {
                DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

                Document doc = docBuilder.newDocument();
                Element facturae = doc.createElement("fe:Facturae");
                Attr attrFE = doc.createAttribute("xmlns:ds");
                attrFE.setValue("http://www.facturae.es/Facturae/2009/v3.2/Facturae");
                Attr attrDS = doc.createAttribute("xmlns:fe");
                attrDS.setValue("http://www.w3.org/2000/09/xmldsig#");
                facturae.setAttributeNode(attrDS);
                facturae.setAttributeNode(attrFE);
                doc.appendChild(facturae);

                //TODO Toda la cabecera
                Node fileHeader = doc.getFirstChild();
                Element cabecera = doc.createElement("FileHeader");
                fileHeader.appendChild(cabecera);

                Element version = doc.createElement("SchemaVersion");
                version.appendChild(doc.createTextNode("3.2"));

                cabecera.appendChild(version);
                Element modality = doc.createElement("Modality");
                modality.appendChild(doc.createTextNode("I"));

                cabecera.appendChild(modality);
                Element issuerType = doc.createElement("InvoiceIssuerType");
                issuerType.appendChild(doc.createTextNode("EM"));
                cabecera.appendChild(issuerType);

                //TODO todo el apartado de Batch
                Element batchTag = doc.createElement("Batch");
                cabecera.appendChild(batchTag);
                Element batchIndentifier = doc.createElement("BatchIdentifier");
                batchIndentifier.setTextContent(cif.getText().toUpperCase() + numFact.getText() + serieFact.getValue());
                batchTag.appendChild(batchIndentifier);
                Element invoicesCount = doc.createElement("InvoicesCount");
                invoicesCount.appendChild(doc.createTextNode("1"));
                batchTag.appendChild(invoicesCount);

                //TODO todos lo Amount con el precio total
                Element totalInvoices = doc.createElement("TotalInvoicesAmount");
                batchTag.appendChild(totalInvoices);
                Element totalAmount = doc.createElement("TotalAmount");
                totalInvoices.appendChild(totalAmount);
                Element totalOutstanding = doc.createElement("TotalOutstandingAmount");
                batchTag.appendChild(totalOutstanding);
                Element totalAmount1 = doc.createElement("TotalAmount");

                totalOutstanding.appendChild(totalAmount1);
                Element totalExecutable = doc.createElement("TotalExecutableAmount");
                batchTag.appendChild(totalExecutable);
                Element totalAmount2 = doc.createElement("TotalAmount");

                totalExecutable.appendChild(totalAmount2);
                Element currencyCode = doc.createElement("InvoiceCurrencyCode");
                currencyCode.appendChild(doc.createTextNode("EUR"));
                batchTag.appendChild(currencyCode);
                //TODO fin del bloque Batch

                //TODO detalles de la los Parties para saber el tipo de persona
                Node parties = doc.getFirstChild();
                Element parties1 = doc.createElement("Parties");
                parties.appendChild(parties1);
                Element seller = doc.createElement("SellerParty");
                parties1.appendChild(seller);
                Element taxIdentification = doc.createElement("TaxIdentification");
                seller.appendChild(taxIdentification);
                Element typePerson = doc.createElement("PersonTypeCode");
                if (personType.getValue() == "Persona Física") {
                    typePerson.setTextContent("F");
                    taxIdentification.appendChild(typePerson);
                } else {
                    typePerson.setTextContent("J");
                    taxIdentification.appendChild(typePerson);
                }
                Element residenceType = doc.createElement("ResidenceTypeCode");
                residenceType.setTextContent("R");
                taxIdentification.appendChild(residenceType);
                Element idNum = doc.createElement("TaxIdentificationNumber");
                idNum.setTextContent("ES" + cif.getText().toUpperCase());
                taxIdentification.appendChild(idNum);

                //TODO información personal
                Node individual = doc.getFirstChild();
                if (personType.getValue() == "Persona Física") {
                    Element tipoPersona = doc.createElement("Individual");
                    seller.appendChild(tipoPersona);
                    Element nombre = doc.createElement("Name");
                    nombre.setTextContent(name.getText().toUpperCase());
                    tipoPersona.appendChild(nombre);
                    Element apellido1 = doc.createElement("FirstSurname");
                    apellido1.setTextContent(surname1.getText().toUpperCase());
                    tipoPersona.appendChild(apellido1);
                    Element apellido2 = doc.createElement("SecondSurname");
                    apellido2.setTextContent(surname2.getText().toUpperCase());
                    tipoPersona.appendChild(apellido2);
                    //TODO detalles de la dirección
                    direccion(doc, individual, tipoPersona);
                } else if (personType.getValue() == "Persona Juridica") {
                    Element tipoPersona = doc.createElement("LegalEntity");
                    individual.appendChild(tipoPersona);
                    Element corporate = doc.createElement("CorporateName");
                    corporate.setTextContent(name.getText().toUpperCase());
                    tipoPersona.appendChild(corporate);
                    //TODO detalles de la dirección
                    direccion(doc, individual, tipoPersona);
                }

                //TODO detalles sobre el ayuntamiento
                Element buyer = doc.createElement("BuyerParty");
                parties1.appendChild(buyer);
                Element taxId = doc.createElement("TaxIdentification");
                buyer.appendChild(taxId);
                Element personTypeCode = doc.createElement("PersonTypeCode");
                personTypeCode.setTextContent("J");
                taxId.appendChild(personTypeCode);
                Element residenceTypeCode = doc.createElement("ResidenceTypeCode");
                residenceTypeCode.setTextContent("R");
                taxId.appendChild(residenceTypeCode);
                Element taxIdNum = doc.createElement("TaxIdentificationNumber");
                taxIdNum.setTextContent("P0309600E");
                taxId.appendChild(taxIdNum);

                Element adOficina = doc.createElement("AdministrativeCentres");
                buyer.appendChild(adOficina);
                //TODO detalles Oficina Contable
                Element adCentre = doc.createElement("AdministrativeCentre");
                adOficina.appendChild(adCentre);
                Element centreCode = doc.createElement("CentreCode");
                centreCode.setTextContent("L01030968");
                adCentre.appendChild(centreCode);
                Element roleType = doc.createElement("RoleTypeCode");
                roleType.setTextContent("01");
                adCentre.appendChild(roleType);
                Element addressAyunt = doc.createElement("AddressInSpain");
                adCentre.appendChild(addressAyunt);
                Element streetAyunt = doc.createElement("Address");
                addressAyunt.appendChild(streetAyunt);
                Element cpAyunt = doc.createElement("PostCode");
                cpAyunt.setTextContent("00000");
                addressAyunt.appendChild(cpAyunt);
                Element townAyunt = doc.createElement("Town");
                townAyunt.setTextContent("");
                addressAyunt.appendChild(townAyunt);
                Element provAyunt = doc.createElement("Province");
                provAyunt.setTextContent("");
                addressAyunt.appendChild(provAyunt);
                Element countryCodeAyunt = doc.createElement("CountryCode");
                countryCodeAyunt.setTextContent("ESP");
                addressAyunt.appendChild(countryCodeAyunt);
                Element centreDesc = doc.createElement("CentreDescription");
                centreDesc.setTextContent("Oficina Contable");
                adCentre.appendChild(centreDesc);

                //TODO detalles Órgano Gestor
                Element adCentre1 = doc.createElement("AdministrativeCentre");
                adOficina.appendChild(adCentre1);
                Element centreCode1 = doc.createElement("CentreCode");
                centreCode1.setTextContent("L01030968");
                adCentre1.appendChild(centreCode1);
                Element roleType1 = doc.createElement("RoleTypeCode");
                roleType1.setTextContent("02");
                adCentre1.appendChild(roleType1);
                Element addressAyunt1 = doc.createElement("AddressInSpain");
                adCentre1.appendChild(addressAyunt1);
                Element streetAyunt1 = doc.createElement("Address");
                streetAyunt1.setTextContent("BARBERÁN Y COLLAR, 15");
                addressAyunt1.appendChild(streetAyunt1);
                Element cpAyunt1 = doc.createElement("PostCode");
                cpAyunt1.setTextContent("03430");
                addressAyunt1.appendChild(cpAyunt1);
                Element townAyunt1 = doc.createElement("Town");
                townAyunt1.setTextContent("ONIL");
                addressAyunt1.appendChild(townAyunt1);
                Element provAyunt1 = doc.createElement("Province");
                provAyunt1.setTextContent("ALICANTE");
                addressAyunt1.appendChild(provAyunt1);
                Element countryCodeAyunt1 = doc.createElement("CountryCode");
                countryCodeAyunt1.setTextContent("ESP");
                addressAyunt1.appendChild(countryCodeAyunt1);
                Element centreDesc1 = doc.createElement("CentreDescription");
                centreDesc1.setTextContent("Órgano Gestor");
                adCentre1.appendChild(centreDesc1);

                //TODO detalles Órgano Gestor
                Element adCentre2 = doc.createElement("AdministrativeCentre");
                adOficina.appendChild(adCentre2);
                Element centreCode2 = doc.createElement("CentreCode");
                centreCode2.setTextContent("L01030968");
                adCentre2.appendChild(centreCode2);
                Element roleType2 = doc.createElement("RoleTypeCode");
                roleType2.setTextContent("03");
                adCentre2.appendChild(roleType2);
                Element addressAyunt2 = doc.createElement("AddressInSpain");
                adCentre2.appendChild(addressAyunt2);
                Element streetAyunt2 = doc.createElement("Address");
                addressAyunt2.appendChild(streetAyunt2);
                Element cpAyunt2 = doc.createElement("PostCode");
                cpAyunt2.setTextContent("00000");
                addressAyunt2.appendChild(cpAyunt2);
                Element townAyunt2 = doc.createElement("Town");
                addressAyunt2.appendChild(townAyunt2);
                Element provAyunt2 = doc.createElement("Province");
                addressAyunt2.appendChild(provAyunt2);
                Element countryCodeAyunt2 = doc.createElement("CountryCode");
                countryCodeAyunt2.setTextContent("ESP");
                addressAyunt2.appendChild(countryCodeAyunt2);
                Element centreDesc2 = doc.createElement("CentreDescription");
                centreDesc2.setTextContent("Unidad Tramitadora");
                adCentre2.appendChild(centreDesc2);


                //TODO aquí van más 'AdministrativeCentre'
                Element legalEntity = doc.createElement("LegalEntity");
                buyer.appendChild(legalEntity);
                Element corporate = doc.createElement("CorparateName");
                corporate.setTextContent("");
                legalEntity.appendChild(corporate);

                Element addressAyunt3 = doc.createElement("AddressInSpain");
                legalEntity.appendChild(addressAyunt3);
                Element streetAyunt3 = doc.createElement("Address");
                streetAyunt3.setTextContent("BARBERÁN Y COLLAR, 15");
                addressAyunt3.appendChild(streetAyunt3);
                Element cpAyunt3 = doc.createElement("PostCode");
                cpAyunt3.setTextContent("03430");
                addressAyunt3.appendChild(cpAyunt3);
                Element townAyunt3 = doc.createElement("Town");
                townAyunt3.setTextContent("ONIL");
                addressAyunt3.appendChild(townAyunt3);
                Element provAyunt3 = doc.createElement("Province");
                provAyunt3.setTextContent("ALICANTE");
                addressAyunt3.appendChild(provAyunt3);
                Element countryCodeAyunt3 = doc.createElement("CountryCode");
                countryCodeAyunt3.setTextContent("ESP");
                addressAyunt3.appendChild(countryCodeAyunt3);

                //TODO todos los detalles de las facturas
                Node invs = doc.getFirstChild();
                Element invoices = doc.createElement("Invoices");
                invs.appendChild(invoices);

                Node inv = doc.getFirstChild();
                Element invoice = doc.createElement("Invoice");
                inv.appendChild(invoice);
                invoices.appendChild(invoice);

                Node invHead = doc.getFirstChild();
                Element invoiceHeader = doc.createElement("InvoiceHeader");
                invHead.appendChild(invoiceHeader);
                invoices.appendChild(invoiceHeader);
                invoice.appendChild(invoiceHeader);

                Element invoiceNumber = doc.createElement("InvoiceNumber");
                invoiceNumber.setTextContent(numFact.getText());
                invoiceHeader.appendChild(invoiceNumber);
                Element invoiceSerie = doc.createElement("InvoiceSeriesCode");
                invoiceSerie.setTextContent(serieFact.getValue());
                invoiceHeader.appendChild(invoiceSerie);
                Element invoiceDoc = doc.createElement("InvoiceDocumentType");
                invoiceDoc.appendChild(doc.createTextNode("FC"));
                invoiceHeader.appendChild(invoiceDoc);
                Element invoiceClass = doc.createElement("InvoiceClass");
                invoiceClass.appendChild(doc.createTextNode("OO"));
                invoiceHeader.appendChild(invoiceClass);

                Node invIssue = doc.getFirstChild();
                Element invoiceIssueData = doc.createElement("InvoiceIssueData");
                invIssue.appendChild(invoiceIssueData);
                invoices.appendChild(invoiceIssueData);
                invoice.appendChild(invoiceIssueData);
                Element issueDate = doc.createElement("IssueData");
                issueDate.setTextContent(String.valueOf(date.getValue()));
                invoiceIssueData.appendChild(issueDate);
                Element invoiceCurrencyCode = doc.createElement("InvoiceCurrencyCode");
                invoiceCurrencyCode.setTextContent("EUR");
                invoiceIssueData.appendChild(invoiceCurrencyCode);
                Element taxCurrencyCode = doc.createElement("TaxCurrencyCode");
                taxCurrencyCode.setTextContent("EUR");
                invoiceIssueData.appendChild(taxCurrencyCode);
                Element lenguage = doc.createElement("LenguageName");
                lenguage.setTextContent("es");
                invoiceIssueData.appendChild(lenguage);
                //TODO información de cada producto (Tax)
                Node taxesOut = doc.getFirstChild();
                Element taxesOutputs = doc.createElement("TaxesOutputs");
                taxesOut.appendChild(taxesOutputs);
                invoices.appendChild(taxesOutputs);
                invoice.appendChild(taxesOutputs);
                for(int i = 0; i < ivaList.size(); i++){
                    Metodos.Tax(doc, i, invoice, taxesOutputs, (ArrayList<String>) ivaList,
                            (ArrayList<Float>) cantList, (ArrayList<Float>) precioList);
                }
                //TODO el TaxesWithHelp es solo si hay IRPF
                if(chIRPF.isSelected()) {
                    Node taxesWith = doc.getFirstChild();
                    Element taxesWithheld = doc.createElement("TaxesWithheld");
                    taxesWith.appendChild(taxesWithheld);
                    invoices.appendChild(taxesWithheld);
                    invoice.appendChild(taxesWithheld);
                    Node tax = doc.getFirstChild();
                    Element taxx = doc.createElement("Tax");
                    tax.appendChild(taxx);
                    invoice.appendChild(taxx);
                    taxesWithheld.appendChild(taxx);
                    Element taxTypeCode = doc.createElement("TaxTypeCode");
                    taxTypeCode.setTextContent("04");
                    taxx.appendChild(taxTypeCode);
                    Element taxRate = doc.createElement("TaxRate");
                    Metodos.porcentajeIRPF(taxRate, cbIRPF);
                    taxx.appendChild(taxRate);
                    Node taxable = doc.getFirstChild();
                    Element taxableBase = doc.createElement("TaxableBase");
                    taxable.appendChild(taxableBase);
                    invoice.appendChild(taxableBase);
                    taxesOutputs.appendChild(taxableBase);
                    taxx.appendChild(taxableBase);
                    Element taxAmount = doc.createElement("TotalAmount");
                    taxAmount.setTextContent(String.valueOf(Metodos.Total()));
                    taxableBase.appendChild(taxAmount);
                    Node taxAmount3 = doc.getFirstChild();
                    Element taxxAmount = doc.createElement("TaxAmount");
                    taxAmount3.appendChild(taxxAmount);
                    invoice.appendChild(taxxAmount);
                    taxesOutputs.appendChild(taxxAmount);
                    taxx.appendChild(taxxAmount);
                    Element totalAmount3 = doc.createElement("TotalAmount");
                    float total1 = Metodos.Total();
                    totalAmount3.setTextContent(String.valueOf(Metodos.calculoIRPF(total1, cbIRPF)));
                    taxxAmount.appendChild(totalAmount3);
                }
                Node invTot = doc.getFirstChild();
                Element invoiceTotals = doc.createElement("InvoiceTotals");
                invTot.appendChild(invoiceTotals);
                invoices.appendChild(invoiceTotals);
                invoice.appendChild(invoiceTotals);
                Element totalGross = doc.createElement("TotalGrossAmount");
                totalGross.setTextContent(String.valueOf(Metodos.Total()));
                invoiceTotals.appendChild(totalGross);
                Element totalGeneral = doc.createElement("TotalGeneralDiscounts");
                totalGeneral.setTextContent("0.00");
                invoiceTotals.appendChild(totalGeneral);
                Element beforeTaxes = doc.createElement("TotalGrossAmountBeforeTaxes");
                beforeTaxes.setTextContent(String.valueOf(Metodos.Total()));
                invoiceTotals.appendChild(beforeTaxes);
                Element totalTaxOutputs = doc.createElement("TotalTaxOutputs");
                float total1 = Metodos.Total(); float irpf = Metodos.calculoIRPF(total1, cbIRPF);
                float suma = Metodos.ivaSum(); float restaIRPF = irpf - suma; float todo = Metodos.Total();
                float restaTotal = todo - restaIRPF;
                //TODO el TotalAmount del principio
                totalAmount.setTextContent(String.valueOf(restaTotal));
                totalAmount1.setTextContent(String.valueOf(restaTotal));
                totalAmount2.setTextContent(String.valueOf(restaTotal));

                totalTaxOutputs.setTextContent(String.valueOf(restaIRPF));
                invoiceTotals.appendChild(totalTaxOutputs);
                Element totalTaxesWithheld = doc.createElement("TotalTaxesWithheld");
                totalTaxesWithheld.setTextContent("0.00");
                invoiceTotals.appendChild(totalTaxesWithheld);
                Element invoiceTotal = doc.createElement("InvoiceTotal");
                invoiceTotal.setTextContent(String.valueOf(restaTotal));
                invoiceTotals.appendChild(invoiceTotal);
                Element totalOutstandingAmount = doc.createElement("TotalOutstandingAmount");
                totalOutstandingAmount.setTextContent(String.valueOf(restaTotal));
                invoiceTotals.appendChild(totalOutstandingAmount);
                Element totalExecutableAmount = doc.createElement("TotalExecutableAmount");
                totalExecutableAmount.setTextContent(String.valueOf(restaTotal));
                invoiceTotals.appendChild(totalExecutableAmount);

                //TODO detalles sobre cada producto en el árbol de Items
                Node item = doc.getFirstChild();
                Element items = doc.createElement("Items");
                item.appendChild(items);
                invoices.appendChild(items);
                invoice.appendChild(items);
                for(int i = 0; i < cantList.size(); i++){
                    Metodos.Items(doc, i, invoice, items, (ArrayList<String>) ivaList,
                            (ArrayList<Float>) cantList, (ArrayList<Float>) precioList, (ArrayList<String>) descList);
                }


                //TODO el payment sobre el pago
                Node payment = doc.getFirstChild();
                Element paymentDetails = doc.createElement("PaymentDetails");
                payment.appendChild(paymentDetails);
                invoice.appendChild(paymentDetails);
                Node instal = doc.getFirstChild();
                Element installment = doc.createElement("Installment");
                instal.appendChild(installment);
                invoice.appendChild(installment);
                paymentDetails.appendChild(installment);
                Element dueDate = doc.createElement("InstallmentDueDate");
                dueDate.setTextContent(String.valueOf(date.getValue()));
                installment.appendChild(dueDate);
                Element installmentAmount = doc.createElement("InstallmentAmount");
                installmentAmount.setTextContent(String.valueOf(restaTotal));
                installment.appendChild(installmentAmount);
                Element paymentMeans = doc.createElement("PaymentMeans");
                paymentMeans.setTextContent("04");
                installment.appendChild(paymentMeans);


                //TODO sobre la cuenta bancaria
                Node account = doc.getFirstChild();
                Element accountCredit = doc.createElement("AccountToBeCredited");
                account.appendChild(accountCredit);
                invoice.appendChild(accountCredit);
                paymentDetails.appendChild(accountCredit);
                installment.appendChild(accountCredit);
                Element iban = doc.createElement("IBAN");
                iban.setTextContent(iban1.getText().toUpperCase() + iban2.getText() + iban3.getText() + iban4.getText()
                        + iban5.getText() + iban6.getText());
                accountCredit.appendChild(iban);
                Element bankCode = doc.createElement("BankCode");
                bankCode.setTextContent(iban2.getText());
                accountCredit.appendChild(bankCode);
                Element banchCode = doc.createElement("BanchCode");
                banchCode.setTextContent(iban3.getText());
                accountCredit.appendChild(banchCode);
                //TODO detalles centro de madrid
                Node addressSpain = doc.getFirstChild();
                Element branchSpain = doc.createElement("BranchInSpainAddress");
                addressSpain.appendChild(branchSpain);
                invoice.appendChild(branchSpain);
                paymentDetails.appendChild(branchSpain);
                accountCredit.appendChild(branchSpain);
                Element address = doc.createElement("Address");
                address.setTextContent("CENTRO PYMES");
                branchSpain.appendChild(address);
                Element cp = doc.createElement("PostCode");
                cp.setTextContent("28760");
                branchSpain.appendChild(cp);
                Element town = doc.createElement("Town");
                town.setTextContent("TRES CANTOS");
                branchSpain.appendChild(town);
                Element province = doc.createElement("Province");
                province.setTextContent("MADRID");
                branchSpain.appendChild(province);
                Element country = doc.createElement("CountryCode");
                country.setTextContent("ESP");
                branchSpain.appendChild(country);

                Node legal = doc.getFirstChild();
                Element legalLiteral = doc.createElement("LegalLiterals");
                legal.appendChild(legalLiteral);
                invoice.appendChild(legalLiteral);
                Element legalReference = doc.createElement("LegalReference");
                legalReference.setTextContent(details.getText());
                legalLiteral.appendChild(legalReference);

                Node additional = doc.getFirstChild();
                Element additionalData = doc.createElement("AdditionalData");
                additional.appendChild(additionalData);
                invoice.appendChild(additionalData);
                Element additionalInfo = doc.createElement("InvoiceAdditionalInformation");
                additionalInfo.setTextContent(details.getText());
                additionalData.appendChild(additionalInfo);


                //TODO transformar la informacion en un fichero xml
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                DOMSource source = new DOMSource(doc);

                Metodos.save(transformer, source, stage);


            } catch (ParserConfigurationException pce) {
                pce.printStackTrace();
            } catch (TransformerException tfe) {
                tfe.printStackTrace();
            }
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        img.setImage(new Image("file:img/logoOnil.png"));

        Metodos.tamanoField(iban1, 4); Metodos.tamanoField(iban2, 4);
        Metodos.tamanoField(iban3, 4); Metodos.tamanoField(iban4, 4);
        Metodos.tamanoField(iban5, 4); Metodos.tamanoField(iban6, 4);
        Metodos.tamanoField(cif, 10); Metodos.tamanoField(postalCode, 5);

        personType.getItems().add("Persona Física"); personType.getItems().add("Persona Juridica");
        serieFact.getItems().add("B"); serieFact.getItems().add("AB");
        serieFact.getItems().add("R"); serieFact.getItems().add("Z");
        cbIVA.getItems().add("21%"); cbIVA.getItems().add("10%");
        cbIVA.getItems().add("4%"); cbIVA.getItems().add("SIN IVA");

        cbIRPF.getItems().add("19%"); cbIRPF.getItems().add("15%"); cbIRPF.getItems().add("7%");

        personType.getSelectionModel().select("Persona Física");
        serieFact.getSelectionModel().select("B");
        cbIRPF.getSelectionModel().select("15%");
        cbIVA.getSelectionModel().select("21%");

        Metodos.onlyNumbers(postalCode); Metodos.onlyNumbers(numFact);
        Metodos.onlyNumbers(iban2); Metodos.onlyNumbers(iban3); Metodos.onlyNumbers(iban4);
        Metodos.onlyNumbers(iban5); Metodos.onlyNumbers(iban6);
        Metodos.onlyDecimals(cant); Metodos.onlyDecimals(precio);

        chIRPF.setSelected(true);

        colCant.setCellValueFactory(new PropertyValueFactory<>("cant"));
        colDesc.setCellValueFactory(new PropertyValueFactory<>("desc"));
        colPrecio.setCellValueFactory(new PropertyValueFactory<>("precio"));
        colIVA.setCellValueFactory(new PropertyValueFactory<>("iva"));

    }

    @FXML
    void Persona(ActionEvent event){
        if(personType.getValue() == "Persona Juridica"){
            surname1.setEditable(false);
            surname1.clear();
            surname2.setEditable(false);
            surname2.clear();
        }else if(personType.getValue() == "Persona Física"){
            surname1.setEditable(true);
            surname2.setEditable(true);
        }
    }

    @FXML
    void HabilitarIRPF(ActionEvent event){
        if(chIRPF.isSelected()){
            chIRPF.setText("Deshabilitar");
            cbIRPF.setDisable(false);
        }else{
            chIRPF.setText("Habilitar");
            cbIRPF.setDisable(true);
        }
    }

    @FXML
    void Delete(ActionEvent event){
        tabla.getItems().removeAll(tabla.getSelectionModel().getSelectedItem());
    }


    @FXML
    void Añadir(ActionEvent event) {
        if(cant.getText() == "" || desc.getText() == "" || precio.getText() == ""){
            String mensaje = "No dejes ningún campo vacío.";
            Alert alert = new Alert(Alert.AlertType.ERROR, mensaje, ButtonType.OK);
            alert.setTitle("Error!");
            alert.setHeaderText(mensaje);
            alert.setContentText("");
            alert.showAndWait();
        }else {

            float decimals2 = Float.parseFloat(cant.getText());
            float decimals3 = Float.parseFloat(precio.getText());

            Producto producto = new Producto(decimals2, desc.getText(), decimals3, cbIVA.getValue());
            tabla.getItems().add(producto);

            cant.clear(); desc.clear(); precio.clear();
        }
    }

    @FXML
    void Enlace(ActionEvent event) {
        Runtime rt = Runtime.getRuntime();
        String url = "https://face.gob.es/es";
        try {
            rt.exec("rundll32 url.dll,FileProtocolHandler " + url);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }


    //TODO detalles sobre la dirección
    private void direccion(Document doc, Node individual, Node tipoPersona) {
        Node direccion = doc.getFirstChild();
        Element address = doc.createElement("AddressInSpain");
        individual.appendChild(address);

        Element calle = doc.createElement("Address");
        calle.setTextContent(direction.getText().toUpperCase());
        address.appendChild(calle);
        Element codigoPostal = doc.createElement("PostCode");
        codigoPostal.setTextContent(postalCode.getText().toUpperCase());
        address.appendChild(codigoPostal);
        Element pueblo = doc.createElement("Town");
        pueblo.setTextContent(city.getText().toUpperCase());
        address.appendChild(pueblo);
        Element provincia = doc.createElement("Province");
        provincia.setTextContent(province.getText().toUpperCase());
        address.appendChild(provincia);
        Element codigoPais = doc.createElement("CountryCode");
        codigoPais.appendChild(doc.createTextNode("ESP"));
        address.appendChild(codigoPais);
        direccion.appendChild(address);
        tipoPersona.appendChild(address);
    }

}