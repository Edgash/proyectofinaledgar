package com.example.proyectoedgarruiz;

public class Producto {
    private float cant;
    private String desc;
    private float precio;
    private String iva;

    public Producto() {
    }

    public Producto(float cant, String desc, float precio, String iva) {
        this.cant = cant;
        this.desc = desc;
        this.precio = precio;
        this.iva = iva;
    }

    public Float getCant() {
        return cant;
    }

    public String getDesc() {
        return desc;
    }

    public Float getPrecio() {
        return precio;
    }

    public String getIva() {
        return iva;
    }
}
